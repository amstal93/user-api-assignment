# User API - Assignment

User API

## Name
User API - Assignment

## Description
A simple NodeJS app that authenticates users and lets them like each other.

It uses ExpressJS, PostgreSQL (using MikroORM) and Jest for testing.

The app also has linting integrated (ESLint).

## Running
* Install Docker at https://docs.docker.com/get-docker/
* Run `yarn` or `npm install` to install the required dependencies
* Run `docker-compose up -d` and this will start up a container containing a PostgreSQL image which we will be using for this project.
* Run `yarn start:dev` or `npm run start:dev` to host the server on your local machine.

## Testing
To run the tests, simply run `yarn test` or `npm run test`.

The tests contain one test file (`route.spec.ts`) for all the routes (including some other cases too) and one test file (`login.spec.ts`) to test the usecases (units) individually, I left that only as an example of how to test usecases in isolation.

## Hooks
The repository contains git hooks (from Husky) in pre-commit.

It lints and tests the application before allowing the contributor to commit their changes.

## Explanations & Arguments

I went with the RDBMS choice for this service since it makes it easier for me to maintain many-to-many relationships (users<->likes). I also like using some particular ORMs and having a clear picture of what is inside my database and the relationships between those entities. RDBMS is a natural choice for developers like me since the relationships are very logical and self-explainable. Querying, constraining and assigning rules to your data is also very simple with relational databases (apart from many other benefits...).

I also prefer relational databases in terms of scaling (in my opinion, they scale better than NoSQL ones)... if however, NoSQL makes sense at some point on any of the app features and since PostgreSQL also has support for the JSON data structure (and so does MikroORM), we can introduce those too in the future (with ease) :)

## License
MIT
