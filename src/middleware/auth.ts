import { Request, Response, NextFunction } from "express";
import jwt from "jsonwebtoken";
import User from "../user/entity";
import getById from "../user/usecases/get-by-id";
import { AuthorizationException } from "../utils/exceptions/exceptions";
import { DI } from "../server";

export const auth = async (req: Request, res: Response, next: NextFunction) => {
  const authHeaders = req.headers.authorization;
  const SECRET = process.env.SECRET || "";
  if (authHeaders && (authHeaders as string).split(" ")[1]) {
    const token = (authHeaders as string).split(" ")[1];
    let decoded: any;

    try {
      decoded = jwt.verify(token, SECRET);
    } catch (error) {
      next(new AuthorizationException("Invalid bearer token."));
    }

    try {
      const user = await getById({
        userRepository: DI.userRepository,
      })(decoded.id);

      req.user = user;
      req.user.id = user.id;
    } catch (error) {
      next(error);
    }

    next();
  } else {
    next(new AuthorizationException("Permission denied, please check your bearer token."));
  }
};
