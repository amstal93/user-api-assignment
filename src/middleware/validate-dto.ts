import { RequestHandler } from "express";
import { plainToInstance } from "class-transformer";
import { validate, ValidationError as ClassValidatorError } from "class-validator";
import { sanitize } from "class-sanitizer";
import { BadRequestException } from "../utils/exceptions/exceptions";

const validateDTO = (type: any, skipMissingProperties = false): RequestHandler => (req, res, next) => {
  const dtoObj = plainToInstance(type, req.body);
  validate(dtoObj, { skipMissingProperties }).then(
    (errors: ClassValidatorError[]) => {
      if (errors.length > 0) {
        const dtoErrors = errors.map((error: ClassValidatorError) => Object.values(error.constraints || [])).join(", ");
        next(new BadRequestException(dtoErrors));
      } else {
        sanitize(dtoObj);
        req.body = dtoObj;
        next();
      }
    },
  );
};

export default validateDTO;
