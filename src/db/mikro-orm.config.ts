import { Options } from "@mikro-orm/core";
import { SqlHighlighter } from "@mikro-orm/sql-highlighter";
import User from "../user/entity";
import UserSubscriber from "../user/subscriber";
import { NotFoundException } from "../utils/exceptions/exceptions";

const options: Options = {
  type: "postgresql",
  entities: [User],
  subscribers: [new UserSubscriber()],
  dbName: process.env.POSTGRES_DB,
  host: process.env.POSTGRES_HOST,
  user: process.env.POSTGRES_USERNAME,
  password: process.env.POSTGRES_PASSWORD,
  highlighter: new SqlHighlighter(),
  debug: true,
  migrations: {
    path: "./dist/db/migrations",
    pathTs: "./src/db/migrations",
    glob: "!(*.d).{js,ts}",
    disableForeignKeys: true,
    allOrNothing: true,
    dropTables: true,
    emit: "ts",
    snapshot: false,
  },
  seeder: {
    path: "./dist/db/seeders",
    pathTs: "./src/db/seeders",
    defaultSeeder: "DatabaseSeeder",
    glob: "!(*.d).{js,ts}",
    emit: "ts",
  },
  findOneOrFailHandler: (entityName: string) => new NotFoundException(entityName),
};

export default options;
