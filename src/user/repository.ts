import { EntityRepository } from "@mikro-orm/core";
import User from "./entity";

export default class UserRepository extends EntityRepository<User> {
  userExists = async (username: string): Promise<boolean> => {
    const exists = await this.findOne({ username });

    return !!exists;
  };

  findByUsername = async (username: string): Promise<User> => this.findOneOrFail({
    username,
  });

  getWithLikes = async (id: number): Promise<User> => this.findOneOrFail(
    id,
    {
      populate: ["likes"],
    },
  );

  listUsersByLikes = async (): Promise<User[]> => {
    const users = await this.findAll({
      populate: ["likes"],
    });

    const sortedUsers = users.sort((user1, user2) => user2.likes.length - user1.likes.length);

    return sortedUsers;
  };
}
