import {
  Entity, Property, Unique, ManyToMany, Collection,
} from "@mikro-orm/core";
import BaseEntity from "../utils/database/base-entity";
import UserRepository from "./repository";

@Entity({ customRepository: () => UserRepository })
export default class User extends BaseEntity {
  @Property({ length: 50 })
  @Unique()
    username!: string;

  @Property({
    hidden: true,
  })
    password!: string;

  @ManyToMany({
    entity: () => User,
    pivotTable: "user_likes",
    joinColumn: "user_liked_id",
    inverseJoinColumn: "user_likes_id",
    hidden: true,
    fixedOrder: true,
    referenceColumnName: "id",
  })
    likes = new Collection<User>(this);

  @Property({
    persist: false,
  })
    numberOfLikes?: number;
}
