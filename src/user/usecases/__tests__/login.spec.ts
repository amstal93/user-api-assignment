import { MikroORM } from "@mikro-orm/core";
import bcrypt from "bcrypt";
import dbOptions from "../../../db/mikro-orm.config";
import { UserSeeder } from "../../../db/seeders/user";
import login from "../login";
import User from "../../entity";
import UserRepository from "../../repository";

let orm: MikroORM;

beforeAll(async () => {
  const testOptions = {
    ...dbOptions,
    dbName: process.env.POSTGRES_DB_TEST,
  };

  orm = await MikroORM.init(testOptions);

  const seeder = orm.getSeeder();
  await orm.getSchemaGenerator().refreshDatabase();

  await seeder.seed(UserSeeder);
});

describe("User Usecase - Signup", () => {
  let userRepository: UserRepository;

  const mockUserRepository = {
    findByUsername: jest.fn().mockImplementation(() => Promise.resolve({
      id: 1,
      username: "test@povio.com",
    })),
    findOneOrFail: jest.fn().mockImplementation(() => Promise.resolve({
      id: 1,
      username: "test@povio.com",
    })),
  };

  beforeEach(() => {
    jest.restoreAllMocks();
    jest.useFakeTimers();
    jest.setSystemTime(new Date());
    jest.spyOn(orm.em, "getRepository").mockImplementation((): any => (mockUserRepository));
    jest.spyOn(bcrypt, "compare").mockImplementation(() => Promise.resolve("success"));
    userRepository = orm.em.getRepository(User);
  });

  it("should log in successfully", async () => {
    const user = await login({
      userRepository,
    })(
      {
        username: "test@povio.com",
        password: "testpovio.com",
      },
    );

    expect(user).toBeDefined();
    expect(user.user).toBeDefined();
    expect(user.user.username).toEqual("test@povio.com");
    expect(user.user.token).toEqual(expect.any(String));
  });
});

afterAll(async () => {
  await orm.close();
});
