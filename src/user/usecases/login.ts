import bcrypt from "bcrypt";
import { LogInDto } from "../dto/log-in";
import UserRepository from "../repository";
import { buildUserRO } from "../shared/blocks";
import { IUserRO } from "../shared/interfaces";
import { BadRequestException } from "../../utils/exceptions/exceptions";

export interface LogInDependency {
  userRepository: UserRepository;
}

export default (dependency: LogInDependency) => {
  const { userRepository } = dependency;

  return async (data: LogInDto): Promise<IUserRO> => {
    const user = await userRepository.findByUsername(data.username);
    const validPassword = await bcrypt.compare(data.password, user.password);

    if (validPassword) {
      return buildUserRO(user);
    }

    throw new BadRequestException("Invalid password.");
  };
};
