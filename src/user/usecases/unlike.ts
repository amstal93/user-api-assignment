import User from "../entity";
import UserRepository from "../repository";

export interface UnlikeDependency {
  userRepository: UserRepository;
}

export default (dependency: UnlikeDependency) => {
  const { userRepository } = dependency;

  return async (who: number, whom: number): Promise<User> => {
    const user = await userRepository.getWithLikes(who);
    const user2 = await userRepository.getWithLikes(whom);

    user2.likes.remove(user);

    await userRepository.flush();

    return user2;
  };
};
