import { wrap } from "@mikro-orm/core";
import User from "../entity";
import UserRepository from "../repository";

export interface GetDependency {
  userRepository: UserRepository;
}

export default (dependency: GetDependency) => {
  const { userRepository } = dependency;

  return async (id: number): Promise<User> => {
    const user = await userRepository.findOneOrFail(id, {
      populate: ["likes"],
    });

    wrap(user).assign({
      numberOfLikes: user.likes.length,
    });

    return user;
  };
};
