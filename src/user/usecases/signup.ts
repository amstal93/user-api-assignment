import { BadRequestException } from "../../utils/exceptions/exceptions";
import { SignUpDto } from "../dto/sign-up";
import User from "../entity";
import UserRepository from "../repository";

export interface SignUpDependency {
  userRepository: UserRepository;
}

export default (dependency: SignUpDependency) => {
  const { userRepository } = dependency;

  return async (data: SignUpDto): Promise<User> => {
    const exists = await userRepository.userExists(data.username);

    if (exists) {
      throw new BadRequestException("This username already exists");
    }

    const user = userRepository.create({
      username: data.username,
      password: data.password,
    });

    await userRepository.persistAndFlush(user);

    return user;
  };
};
