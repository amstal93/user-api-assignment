import UserRepository from "../repository";
import { buildUserRO } from "../shared/blocks";
import { IUserRO } from "../shared/interfaces";

export interface GetDependency {
  userRepository: UserRepository;
}

export default (dependency: GetDependency) => {
  const { userRepository } = dependency;

  return async (id: number): Promise<IUserRO> => {
    const user = await userRepository.findOneOrFail(id);

    return buildUserRO(user);
  };
};
