import { BadRequestException } from "../../utils/exceptions/exceptions";
import User from "../entity";
import UserRepository from "../repository";

export interface LikeDependency {
  userRepository: UserRepository;
}

export default (dependency: LikeDependency) => {
  const { userRepository } = dependency;

  return async (who: number, whom: number): Promise<User> => {
    const user = await userRepository.getWithLikes(who);
    const user2 = await userRepository.getWithLikes(whom);
    if (user === user2) {
      throw new BadRequestException("You can't like yourself :)");
    }

    user2.likes.add(user);

    await userRepository.flush();

    return user2;
  };
};
