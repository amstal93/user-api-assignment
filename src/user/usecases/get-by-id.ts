import User from "../entity";
import UserRepository from "../repository";

export interface GetDependency {
  userRepository: UserRepository;
}

export default (dependency: GetDependency) => {
  const { userRepository } = dependency;

  return async (id: number): Promise<User> => userRepository.findOneOrFail(id);
};
