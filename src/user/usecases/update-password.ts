import { BadRequestException } from "../../utils/exceptions/exceptions";
import { UpdatePasswordDTO } from "../dto/update-password";
import User from "../entity";
import UserRepository from "../repository";

export interface UpdatePasswordDependency {
  userRepository: UserRepository;
}

export default (dependency: UpdatePasswordDependency) => {
  const { userRepository } = dependency;

  return async (id: number, data: UpdatePasswordDTO): Promise<User> => {
    const user = await userRepository.findOneOrFail(id);

    if (data.password !== data.confirmPassword) {
      throw new BadRequestException("The passwords must match.");
    }

    userRepository.assign(user, {
      password: data.password,
    });

    await userRepository.flush();

    return user;
  };
};
