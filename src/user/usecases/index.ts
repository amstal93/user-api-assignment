import { MikroORM } from "@mikro-orm/core";
import User from "../entity";
import get from "./get";
import signup from "./signup";
import login from "./login";
import updatePassword from "./update-password";
import like from "./like";
import unlike from "./unlike";
import mostLiked from "./most-liked";
import { SignUpDto } from "../dto/sign-up";
import { UpdatePasswordDTO } from "../dto/update-password";
import { LogInDto } from "../dto/log-in";
import { IUserRO } from "../shared/interfaces";
import me from "./me";

export interface IUserUsecase {
  get(id: number): Promise<User>;
  signup(data: SignUpDto): Promise<User>;
  login(data: LogInDto): Promise<IUserRO>;
  me(id: number): Promise<IUserRO>;
  updatePassword(id: number, data: UpdatePasswordDTO): Promise<User>;
  like(who: number, whom: number): Promise<User>;
  unlike(who: number, whom: number): Promise<User>;
  mostLiked(): Promise<User[]>;
}

export default (conn: MikroORM): IUserUsecase => ({
  get: get({
    userRepository: conn.em.getRepository(User),
  }),
  signup: signup({
    userRepository: conn.em.getRepository(User),
  }),
  login: login({
    userRepository: conn.em.getRepository(User),
  }),
  me: me({
    userRepository: conn.em.getRepository(User),
  }),
  updatePassword: updatePassword({
    userRepository: conn.em.getRepository(User),
  }),
  like: like({
    userRepository: conn.em.getRepository(User),
  }),
  unlike: unlike({
    userRepository: conn.em.getRepository(User),
  }),
  mostLiked: mostLiked({
    userRepository: conn.em.getRepository(User),
  }),
});
