import { wrap } from "@mikro-orm/core";
import User from "../entity";
import UserRepository from "../repository";

export interface MostLikedDependency {
  userRepository: UserRepository;
}

export default (dependency: MostLikedDependency) => {
  const { userRepository } = dependency;

  return async (): Promise<User[]> => {
    const users = await userRepository.listUsersByLikes();

    for (const user of users) {
      wrap(user).assign({
        numberOfLikes: user.likes.length,
      });
    }

    return users;
  };
};
