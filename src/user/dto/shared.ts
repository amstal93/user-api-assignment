import { IsNotEmpty, MaxLength, MinLength } from "class-validator";

export class BaseUserAuthDto {
  @IsNotEmpty()
  @MaxLength(50)
  readonly username!: string;

  @IsNotEmpty()
  @MinLength(8)
  readonly password!: string;
}
