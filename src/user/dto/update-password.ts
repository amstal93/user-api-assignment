import { IsNotEmpty } from "class-validator";

export class UpdatePasswordDTO {
  @IsNotEmpty()
  readonly password!: string;

  @IsNotEmpty()
  readonly confirmPassword!: string;
}
