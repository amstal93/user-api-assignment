import request from "supertest";
import { UserSeeder } from "../../db/seeders/user";
import { app, DI, init } from "../../server";

describe("User Routes", () => {
  beforeAll(async () => {
    await init;
    DI.orm.config.set("dbName", process.env.POSTGRES_DB_TEST);
    DI.orm.config.getLogger().setDebugMode(false);
    DI.orm.config.set("allowGlobalContext", true);
    await DI.orm.config.getDriver().reconnect();

    await DI.orm.getSchemaGenerator().refreshDatabase();
    const seeder = DI.orm.getSeeder();

    await seeder.seed(UserSeeder);

    DI.orm.em.clear();
  });

  afterAll(async () => {
    await DI.orm.close(true);
    DI.server.close();
  });

  let jwtToken: string;
  let userId: number;
  const TEST_USERNAME = "test@povio.com";

  it("signup", async () => {
    await request(app)
      .post("/signup")
      .send({
        username: TEST_USERNAME,
        password: "testpoviocom",
      })
      .then((res) => {
        expect(res.status).toEqual(200);
        expect(res.body.id).toEqual(expect.any(Number));
        expect(res.body.username).toEqual(TEST_USERNAME);

        userId = res.body.id;
      });
  });

  it("login", async () => {
    await request(app)
      .post("/login")
      .send({
        username: TEST_USERNAME,
        password: "testpoviocom",
      })
      .then((res) => {
        expect(res.status).toEqual(200);
        expect(res.body.user).toBeDefined();
        expect(res.body.user.username).toEqual(TEST_USERNAME);
        expect(res.body.user.token).toEqual(expect.any(String));

        jwtToken = res.body.user.token;
      });
  });

  it("me", async () => {
    await request(app)
      .get("/me")
      .set("Authorization", `Bearer ${jwtToken}`)
      .then((res) => {
        expect(res.status).toEqual(200);
        expect(res.body.user).toBeDefined();
        expect(res.body.user.username).toEqual(TEST_USERNAME);
        expect(res.body.user.token).toEqual(expect.any(String));
      });
  });

  it("update password", async () => {
    await request(app)
      .patch("/me/update-password")
      .set("Authorization", `Bearer ${jwtToken}`)
      .send({
        password: "poviotestnew",
        confirmPassword: "poviotestnew",
      })
      .then((res) => {
        console.log(res);
        expect(res.status).toEqual(200);
        expect(res.body.id).toEqual(expect.any(Number));
        expect(res.body.username).toEqual(TEST_USERNAME);
      });
  });

  it("update password fail", async () => {
    await request(app)
      .patch("/me/update-password")
      .set("Authorization", `Bearer ${jwtToken}`)
      .send({
        password: "poviotestnew",
        confirmPassword: "poviotestnew2",
      })
      .then((res) => {
        expect(res.status).toEqual(400);
        expect(res.body.status).toEqual(400);
        expect(res.body.name).toEqual("Bad Request");
        expect(res.body.message).toEqual("The passwords must match.");
      });
  });

  it("get", async () => {
    await request(app)
      .get(`/user/${userId}`)
      .then((res) => {
        expect(res.status).toEqual(200);
        expect(res.body.id).toEqual(userId);
        expect(res.body.username).toEqual(TEST_USERNAME);
        expect(res.body.numberOfLikes).toEqual(expect.any(Number));
      });
  });

  it("like", async () => {
    await request(app)
      .post("/user/1/like")
      .set("Authorization", `Bearer ${jwtToken}`)
      .then((res) => {
        expect(res.status).toEqual(200);
        expect(res.body.id).toEqual(expect.any(Number));
        expect(res.body.username).toEqual(expect.any(String));
      });
  });

  it("like fail", async () => {
    await request(app)
      .post(`/user/${userId}/like`)
      .set("Authorization", `Bearer ${jwtToken}`)
      .then((res) => {
        expect(res.status).toEqual(400);
        expect(res.body.status).toEqual(400);
        expect(res.body.name).toEqual("Bad Request");
        expect(res.body.message).toEqual("You can't like yourself :)");
      });
  });

  it("unlike", async () => {
    await request(app)
      .post("/user/1/unlike")
      .set("Authorization", `Bearer ${jwtToken}`)
      .then((res) => {
        expect(res.status).toEqual(200);
        expect(res.body.id).toEqual(expect.any(Number));
        expect(res.body.username).toEqual(expect.any(String));
      });
  });

  it("most liked", async () => {
    await request(app)
      .get("/most-liked")
      .then((res) => {
        expect(res.status).toEqual(200);
        expect(res.body[0]).toBeDefined();
        expect(res.body[0].id).toEqual(expect.any(Number));
        expect(res.body[0].username).toEqual(expect.any(String));
        expect(res.body[0].numberOfLikes).toEqual(expect.any(Number));
      });
  });

  it("authorization error", async () => {
    await request(app)
      .get("/me")
      .then((res) => {
        expect(res.status).toEqual(403);
        expect(res.body.name).toEqual("Authorization Error");
        expect(res.body.status).toEqual(403);
        expect(res.body.message).toEqual("Permission denied, please check your bearer token.");
      });
  });

  it("non-existent route", async () => {
    await request(app)
      .get("/non/existent/route")
      .then((res) => {
        expect(res.status).toEqual(404);
        expect(res.body.message).toEqual("No route found");
      });
  });
});
