import {
  NextFunction, Router, Request, Response,
} from "express";
import { DI } from "../server";
import validateDTO from "../middleware/validate-dto";
import { SignUpDto } from "./dto/sign-up";
import { LogInDto } from "./dto/log-in";
import usecase from "./usecases";
import { UpdatePasswordDTO } from "./dto/update-password";
import { auth } from "../middleware/auth";

const router = Router();

router.post(
  "/signup",
  validateDTO(SignUpDto),
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      const user = await usecase(DI.orm).signup(req.body);

      res.send(user);
    } catch (error) {
      next(error);
    }
  },
);

router.post(
  "/login",
  validateDTO(LogInDto),
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      const user = await usecase(DI.orm).login(req.body);

      res.send(user);
    } catch (error) {
      next(error);
    }
  },
);

router.get(
  "/me",
  auth,
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      const user = await usecase(DI.orm).me(req.user.id);

      res.send(user);
    } catch (error) {
      next(error);
    }
  },
);

router.patch(
  "/me/update-password",
  validateDTO(UpdatePasswordDTO),
  auth,
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      const user = await usecase(DI.orm).updatePassword(
        req.user.id,
        req.body,
      );

      res.send(user);
    } catch (error) {
      next(error);
    }
  },
);

router.get("/user/:id(\\d+)", async (req: Request, res: Response, next: NextFunction) => {
  try {
    const user = await usecase(DI.orm).get(+req.params.id);

    res.send(user);
  } catch (error) {
    next(error);
  }
});

router.post(
  "/user/:id(\\d+)/like",
  auth,
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      const user = await usecase(DI.orm).like(req.user.id, +req.params.id);

      res.send(user);
    } catch (error) {
      next(error);
    }
  },
);

router.post(
  "/user/:id(\\d+)/unlike",
  auth,
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      const user = await usecase(DI.orm).unlike(req.user.id, +req.params.id);

      res.send(user);
    } catch (error) {
      next(error);
    }
  },
);

router.get("/most-liked", async (req: Request, res: Response, next: NextFunction) => {
  try {
    const user = await usecase(DI.orm).mostLiked();

    res.send(user);
  } catch (error) {
    next(error);
  }
});

export default router;
